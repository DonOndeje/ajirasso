<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Http;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        $key = $request->get('session');
        $client = new \GuzzleHttp\Client();
        $response =   Http::get('http://188.166.157.234:8090/api/user/' . $key);
        $user_details = json_decode((string) $response->getBody(), true);

        return view('home', compact('user_details'));
    }
}
