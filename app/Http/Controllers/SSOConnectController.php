<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class SSOConnectController extends Controller
{
    public function index()
    {
        $client = new \GuzzleHttp\Client();
        $response =   Http::post('http://login.ajiradigital.go.ke:8090/api/login/session', [
            // 'headers' => [
            //     'Content-Type' => 'application/json',
            // ],
            // 'form_params' => [
            "client_id" => "1",
            "client_secret" => "9be57031-63e3-11ea-9c4f-9a641bbda339",
            "callback_url" => "http://sso.ajiradigital.go.ke/home",
            // ]

        ]);
        $user_details = json_decode((string) $response->getBody(), true);
        $token = $user_details['body']['token'];
        return redirect('http://login.ajiradigital.go.ke:8090/login?session=' . $token);
    }
}
