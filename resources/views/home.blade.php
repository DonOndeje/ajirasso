@extends('layouts.app')
@section('content')
<div class="container">
<nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
  <a class="navbar-brand" href="#">
         Log In with Ajira Digital Test
  </a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
            <span class="navbar-toggler-icon"></span>
  </button>
<div class="collapse navbar-collapse" id="navbarSupportedContent">
<!-- Right Side Of Navbar -->
<ul class="navbar-nav ml-auto">
 <li class="nav-item dropdown">
    <ul class="navbar-nav mr-auto">
        <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#logoutdropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
             {!! $user_details['body']['first_name']  !!}  {!! $user_details['body']['last_name']  !!}<span class="caret"></span>
        </a>
    </ul>
    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown" id='logoutdropdown'>
                <a class="dropdown-item" href="{{ route('logout') }}"
                            onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();">
                    {{ __('Logout') }}
                </a>
       <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
        </form>
    </div>
 </li>
</ul>
</div>
</nav>
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
