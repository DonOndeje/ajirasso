<!DOCTYPE html>
<html lang="en">
<head>
	<title>Login with Ajira Digital</title>
	<!-- Meta -->
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="Login with Ajira Digital">
	<meta name="author" content="Don Ondeje">
	<link rel="shortcut icon" href="favicon.ico">
    <!--bootstrap-->
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
	<!-- Google Font -->
	<link href="https://fonts.googleapis.com/css?family=Montserrat:800|Roboto:400,500,700&display=swap" rel="stylesheet">
	<!-- FontAwesome JS-->
	<script defer src="/fontawesome/js/all.min.js"></script>
	<!-- Theme CSS -->
	<link id="theme-style" rel="stylesheet" href="/css/theme-4.css">
    <style>
  .btn-label {position: relative;left: -12px;display: inline-block;padding: 6px 12px;background: rgba(0,0,0,0.15);border-radius: 3px 0 0 3px;}
  .btn-labeled {padding-top: 0;padding-bottom: 0;}
  .btn { margin-bottom:10px; }
    </style>
</head>
    <body>
	<header class="header">
		<section class="hero-section">
			<div class="hero-mask">
			</div><!--//hero-mask-->
			<div class="container text-center py-5">
				<div class="single-col-max mx-auto">
					{{-- <div class="hero-heading-upper pt-3 mb-3">Promote Your Online Course <br class="d-md-none">Like A Pro</div> --}}
					<h1 class="hero-heading mb-5">
                        {{-- <div class="site-logo"><a class="navbar-brand" href="/"><img class="logo-icon mr-2" src="{!! asset('images/logo-transparent.png') !!}" alt="logo" style=" height: 40px;"></a></div> --}}
						<span class="brand mb-4 d-block"><span class="text-highlight pr-2">The Future Works Online</span></span>
				    </h1>
					<div class="text-center">
                    {{-- <button type="button" class="btn btn-labeled btn-success">
                  <span class="btn-label"><img class="logo-icon mr-2 tria" src="{!! asset('images/logo-transparent3.jpeg') !!}" alt="logo" style=" height:40px;"></span>Login with Ajira</button> --}}
                      <a href="{!! route('ajira.login') !!}"><img class="logo-icon mr-2 tria" src="{!! asset('images/buttonimage.jpeg') !!}" style="border-radius:7px;height:60px;"></a>
                      {{-- <a href="{!! route('ajira.login') !!}" style=" height:60px;" type="button" id='loginWithAjira' class="btn btn-primary btn-lg ajiraButton"><span class='imagespan' ><img class="logo-icon mr-2 tria" src="{!! asset('images/logo-transparent3.jpeg') !!}" alt="logo" style=" height:40px;"></span>Login in with Ajira</a> --}}
					</div>
				</div><!--//single-col-max-->
			</div><!--//container-->
          <div>
		</section><!--//hero-section-->
	</header><!--//header-->
     <div id="section-faq" class="section-faq my-lg-5">
		        <div class="container">
			        <div class="container-inner p-5 theme-bg-light rounded">
				        <div class="row">
					        <div class="col-12 col-lg-3">
						        <h3 class="section-title text-left mb-4">FAQ</h3>
						        <div class="intro mb-5 pr-lg-3">Can't find the answer you're looking for? Feel free to <a class="theme-link" href="#">get in touch</a>.</div>
					        </div>
					        <div class="col-12 col-lg-9">
						        <div class="faq-items pl-lg-3">
							        <div class="item">
										<h4 class="faq-q mb-2"><i class="far fa-question-circle mr-2 text-primary"></i>What lorem ipsum dolor sit amet?</h4>
										<div class="faq-a">
											<p>Sed venenatis porta ante, nec accumsan leo suscipit ac. Praesent ultricies tortor nisi, eu convallis ex lacinia ac. Praesent vel risus eu ligula ullamcorper condimentum eu ac leo. Praesent leo odio <a href="#">link example</a> interdum vitae mi vitae, maximus porta lectus. Maecenas venenatis, felis quis rutrum luctus, tortor turpis maximus lacus, at scelerisque nisl metus nec augue.  </p>
										</div>
									</div><!--//item-->
									<div class="item">
										<h4 class="faq-q mb-2"><i class="far fa-question-circle mr-2 text-primary"></i>How to lorem ipsum dolor sit amet?</h4>
										<div class="faq-a">
											<p>Donec tincidunt porttitor dictum. Cras laoreet ipsum vitae massa suscipit, at pretium justo molestie. Duis gravida vitae dui vel posuere. Maecenas pharetra, odio nec interdum efficitur, eros magna bibendum tortor, at pellentesque nunc quam eu diam. </p>
										</div>
									</div><!--//item-->
									<div class="item">
										<h4 class="faq-q mb-2"><i class="far fa-question-circle mr-2 text-primary"></i>Does lorem ipsum dolor sit amet?</h4>
										<div class="faq-a">
											<p>Maecenas felis mauris, pharetra at congue sed, semper et orci. Suspendisse maximus viverra tellus vel dictum. Cras lacinia lectus magna, facilisis congue lacus tristique non. </p>
										</div>
									</div><!--//item-->
									<div class="item">
										<h4 class="faq-q mb-2"><i class="far fa-question-circle mr-2 text-primary"></i>When do you lorem ipsum dolor sit amet?</h4>
										<div class="faq-a">
											<p>Suspendisse gravida gravida orci ut egestas. In in libero faucibus tortor blandit iaculis a fermentum lectus. Proin dictum lacus id fringilla interdum.  </p>
										</div>
									</div><!--//item-->
									<div class="item">
										<h4 class="faq-q mb-2"><i class="far fa-question-circle mr-2 text-primary"></i>Can I lorem ipsum dolor sit amet?</h4>
										<div class="faq-a">
											<p>Nam feugiat quam nec ex consectetur volutpat. Phasellus urna diam, finibus non enim id, placerat facilisis orci. Maecenas tristique orci sit amet sem suscipit, vitae auctor lectus pellentesque. </p>
										</div>
									</div><!--//item-->
									<div class="item">
										<h4 class="faq-q mb-2"><i class="far fa-question-circle mr-2 text-primary"></i>Does lorem ipsum dolor sit amet?</h4>
										<div class="faq-a">
											<p>Nam feugiat quam nec ex consectetur volutpat. Phasellus urna diam, finibus non enim id, placerat facilisis orci. Maecenas tristique orci sit amet sem suscipit, vitae auctor lectus pellentesque. </p>
										</div>
									</div><!--//item-->
						        </div><!--//faq-items-->
					        </div>
				        </div><!--//row-->
			        </div><!--//container-inner-->
		        </div><!--//container-->
	        </div><!--//section-faq-->
	<!-- Javascript -->
	<script src="/plugins/jquery-3.4.1.min.js"></script>
	<script src="/plugins/bootstrap/js/bootstrap.min.js"></script>
	<script src="/plugins/back-to-top.js"></script>
    <script src="/plugins/jquery.scrollTo.min.js"></script>
	<script src="/js/main.js"></script>

{{-- <script>
$('#loginWithAjira').on('click', function(e) {
	e.preventDefault();
	window.location = "http://188.166.157.234:8090/api/login/session";
});
</script> --}}
</body>
</html>

